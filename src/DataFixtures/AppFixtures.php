<?php

namespace App\DataFixtures;

use Faker\Factory;
use Faker\Generator;
use App\Entity\Student;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class AppFixtures extends Fixture
{
    private Generator $faker;
    public function __construct()
    {
        $this->faker = Factory::create();
    }
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i <= 15; $i++) {
            $student = new Student();
            $student->setNom($this->faker->lastName())
                ->setPrenom($this->faker->firstName())
                ->setDatedenaissance($this->faker->dateTimeBetween('-30 years', '-18 years'));
            $manager->persist($student);
        }
        $manager->flush();
    }
}
