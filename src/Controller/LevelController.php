<?php

namespace App\Controller;

use App\Entity\Level;
use App\Form\LevelType;
use App\Repository\LevelRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class LevelController extends AbstractController
{
    /**
     * afficher tout les nieveau de classe
     *
     * @param LevelRepository $levelRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    #[Route('/level/all', name: 'app_level')]
    public function index(LevelRepository $levelRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $pagination = $paginator->paginate(
            $levelRepository->findAll(),
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('pages/level/index.html.twig', ['page_title' => 'Classes',
            'levels' => $pagination,
            'allLevel' => $levelRepository->findAll()
        ]);
    }

    /**
     * cette fonction creer une nouvel classe
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('level/new',name:'app_level_new', methods:['GET', 'POST'])]
    public function new(LevelRepository $levelRepository,Request $request, EntityManagerInterface $manager): Response
    {
        $level = new Level();
        $form = $this->createForm(LevelType::class, $level);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $level = $form->getData();
            $nomLevel = $level->getLibelle();
            $manager->persist($level);
            $manager->flush();
            $this->addFlash(
                'success',
                'L\'ajout ' . $nomLevel . ' a été réussie.'
            );
            return $this->redirectToRoute('app_level');
        }
        return $this->render('pages/level/new.html.twig', ['page_title' => 'Nouveau','form' => $form->createView(),'allLevel' => $levelRepository->findAll()]);
    }
    /**
     * cette controller faire une modification de classe
     */
    #[Route('/level/edit/{id}', name: 'app_level_edit', methods: ['GET', 'POST'])]
    public function edit(LevelRepository $repository, Request $request, EntityManagerInterface $manager, int $id): Response
    {
        $level = $repository->findOneBy(['id' => $id]);
        $nomLevel = $level->getLibelle();
        if ($level === null) {
            $this->addFlash('error', 'Cet classe n\'existe pas !');
            return $this->redirectToRoute('app_level_all');
        } else {
            $form = $this->createForm(LevelType::class, $level);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $level = $form->getData();
                $manager->persist($level);
                $manager->flush();
                $this->addFlash(
                    'success',
                    'Vous avez modifié la classe dont le nom est ' . $nomLevel . '.'
                );
                return $this->redirectToRoute('app_level');
            }
        }


        return $this->render('pages/level/edit.html.twig', ['form' => $form->createView(),
        'page_title' => 'Edit level','allLevel' => $repository->findAll()
    ]);
    }
    #[Route('/level/delete/{id}', name: 'app_level_delete', methods: ['GET'])]
    public function delete(EntityManagerInterface $entityManager, int $id, LevelRepository $levelRepository): Response
    {
        $level = $levelRepository->find($id);
        $nomLevel = $level->getLibelle();
        if ($level === null) {
            $this->addFlash('error', 'Cet élève n\'existe pas !');
        } else {
            $entityManager->remove($level);
            $entityManager->flush();
            $this->addFlash('success', 'Le niveau ' . $nomLevel . ' a été supprimé avec succès.');
        }
        return $this->redirectToRoute('app_level');
    }
}
