<?php

namespace App\Controller;

use App\Repository\LevelRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(LevelRepository $levelRepository): Response
    {
        return $this->render('pages/home/index.html.twig', [
            'page_title' => 'Accueil',
            'allLevel' => $levelRepository->findAll()
        ]);
    }
}
