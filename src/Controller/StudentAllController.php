<?php

namespace App\Controller;

use App\Entity\Student;
use App\Form\StudentType;
use App\Repository\LevelRepository;
use App\Repository\StudentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class StudentAllController extends AbstractController
{
    /**
     * Afficher tout les student
     *
     * @param StudentRepository $studentRepository
     * @param PaginatorInterface $paginator
     * @param Request $request
     * @return Response
     */
    #[Route('/student/all', name: 'app_student_all')]
    public function index(StudentRepository $studentRepository,LevelRepository $levelRepository, PaginatorInterface $paginator, Request $request): Response
    {
        $pagination = $paginator->paginate(
            $studentRepository->findAll(),
            $request->query->getInt('page', 1),
            10
        );
        return $this->render('pages/student_all/index.html.twig', ['page_title' => 'Eleves',
        'students' => $pagination,'allLevel' => $levelRepository->findAll()]
    );
    }

    /**
     * ajouter un student
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/student/new', name: 'app_student_new', methods: ['GET', 'POST'])]
    public function new(Request $request,LevelRepository $levelRepository, EntityManagerInterface $manager): Response
    {
        $student = new Student();
        $form = $this->createForm(StudentType::class, $student);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $student = $form->getData();
            $nomStudent = $student->getNom();
            $manager->persist($student);
            $manager->flush();
            $this->addFlash(
                'success',
                'L\'inscription de ' . $nomStudent . ' a été réussie.'
            );
            return $this->redirectToRoute('app_student_all');
        }
        return $this->render('pages/student_all/new.html.twig', ['allLevel' => $levelRepository->findAll(),'page_title' => 'Eleves','form' => $form->createView()]);
    }

    /**
     * modifier un student
     * @param StudentRepository $repository
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/student/edit/{id}', name: 'app_student_edit', methods: ['GET', 'POST'])]
    public function edit(StudentRepository $repository,LevelRepository $levelRepository, Request $request, EntityManagerInterface $manager, int $id): Response
    {
        $student = $repository->findOneBy(['id' => $id]);
        $numStudent = $student->getId();
        if ($student === null) {
            $this->addFlash('error', 'Cet élève n\'existe pas !');
            return $this->redirectToRoute('app_student_all');
        } else {
            $form = $this->createForm(StudentType::class, $student);
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $student = $form->getData();
                $manager->persist($student);
                $manager->flush();
                $this->addFlash(
                    'success',
                    'Vous avez modifié l\'élève dont le numéro d\'inscription est ' . $numStudent . '.'
                );
                return $this->redirectToRoute('app_student_all');
            }
        }


        return $this->render('pages/student_all/edit.html.twig', ['allLevel' => $levelRepository->findAll(),'page_title' => 'Eleves','form' => $form->createView()]);
    }

    /**
     * 
     */
    #[Route('/student/delete/{id}', name: 'app_student_delete', methods: ['GET'])]
    public function delete(EntityManagerInterface $entityManager, int $id, StudentRepository $studentRepository): Response
    {
        $student = $studentRepository->find($id);
        $nomStudent = $student->getNom();
        if ($student === null) {
            $this->addFlash('error', 'Cet élève n\'existe pas !');
        } else {
            $entityManager->remove($student);
            $entityManager->flush();
            $this->addFlash('success', 'L\'éleve ' . $nomStudent . ' a été supprimé avec succès.');
        }
        return $this->redirectToRoute('app_student_all');
    }
}
