<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Form\UserTypePassword;
use App\Repository\UserRepository;
use App\Repository\LevelRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserController extends AbstractController
{
    /**
     * controller pour modifier profil utilisateur
     *
     * @param UserPasswordHasherInterface $hasher
     * @param Request $request
     * @param integer $id
     * @param EntityManagerInterface $manager
     * @param UserRepository $repository
     * @return Response
     */
    #[Route('/user/edit_profil/{id}', name: 'app_user_edit')]
    public function edit(UserPasswordHasherInterface $hasher,LevelRepository $levelRepository, Request $request, int $id, EntityManagerInterface $manager, UserRepository $repository): Response
    {
        $user = $repository->findOneBy(['id' => $id]);
        if (!$this->getUser()) {
            return $this->redirectToRoute('security.login');
        }
        if ($this->getUser() !== $user) {
            $this->addFlash(
                'error',
                'Vous ne pouvez pas modifier le profil d\'un autre utilisateur.'
            );
            return $this->redirectToRoute('app_home');
        }
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($hasher->isPasswordValid($user, $form->getData()->getPlainpassword())) {
                $user = $form->getData();
                $manager->persist($user);
                $manager->flush();
                $this->addFlash(
                    'success',
                    'Modification effectuée avec succès.'
                );
                return $this->redirectToRoute('app_home');
            } else {
                $this->addFlash(
                    'error',
                    'Mot de passe incorrect.'
                );
            }
        }
        return $this->render('pages/user/edit_profil.html.twig', ['allLevel' => $levelRepository->findAll(),'page_title' => 'Utilisateur','form' => $form->createView()]);
    }

    #[Route('/user/edit_password/{id}', name: 'app_user_edit_password')]
    /**
     * ce controller modifie le mot de passe d'un utilisateur
     *
     * @param UserPasswordHasherInterface $hasher
     * @param Request $request
     * @param integer $id
     * @param EntityManagerInterface $manager
     * @param UserRepository $repository
     * @return Response
     */
    public function editPassword(UserPasswordHasherInterface $hasher,LevelRepository $levelRepository, Request $request, int $id, EntityManagerInterface $manager, UserRepository $repository): Response
    {
        $user = $repository->findOneBy(['id' => $id]);
        if (!$this->getUser()) {
            return $this->redirectToRoute('security.login');
        }
        if ($this->getUser() !== $user) {
            $this->addFlash(
                'error',
                'Vous ne pouvez pas modifier le mot de passe d\'un autre utilisateur.'
            );
            return $this->redirectToRoute('app_home');
        }
        $form = $this->createForm(UserTypePassword::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $plainPassword = $form->get('plainPassword')->getData();
            if ($hasher->isPasswordValid($user, $plainPassword)) {
                $newPassword = $form->get('newPassword')->getData();
                $newPasswordHasher= $hasher->hashPassword($user,$newPassword);
                $user->setPassword($newPasswordHasher);
                $manager->persist($user);
                $manager->flush();
                $this->addFlash(
                    'success',
                    'Modification effectuée avec succès.'
                );
                return $this->redirectToRoute('app_home');
            } else {
                $this->addFlash(
                    'error',
                    'Mot de passe incorrect.'
                );
            }
        }
        return $this->render('pages/user/edit_motdepasse.html.twig', ['allLevel' => $levelRepository->findAll(),'page_title' => 'Utilisateur','form' => $form->createView()]);
    }
}
