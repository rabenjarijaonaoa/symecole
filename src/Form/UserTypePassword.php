<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class UserTypePassword extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
        ->add('plainPassword', PasswordType::class, [
            'attr' => [
                'class' => 'form-control form-control-user',
                'placeholder' => 'Ancien mot de passe'
            ]
        ])->add('newPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'mapped' => false,
            'first_options' => [
                'attr' => [
                    'class' => 'form-control form-control-user',
                    'placeholder' => 'Entrez le nouveau de passe'
                ],'label' => false
            ],
            'second_options' => [
                'attr' => [
                    'class' => 'form-control form-control-user mt-3',
                    'placeholder' => 'Répétez le nouveau de passe'
                ],'label' => false
            ],
            'invalid_message' => 'Les champs de mot de passe doivent correspondre.',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
